Maven example
=============

Start a project:

    $ mvn -B archetype:generate \
        -DarchetypeGroupId=org.apache.maven.archetypes \
        -DgroupId=example \
        -DartifactId=app

Test the project

    $ mvn test
